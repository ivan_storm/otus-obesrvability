Мануал по установке elasticsearch и kibana - https://filipporigoni.medium.com/install-elk-stack-8-on-debian-12-37d52c824d3a
1. Устанавливаем Nginx и Mariadb для выполнения ДЗ
2. Устанавливаем elasticsearch, kibana, а также filebeat, metricbeat и hearbeat
3. Генерируем пароль для учетки elastic, генерируем необходимые токены и применяем конфигурации Elasticsearch и kibana 
4. Подключаем модули nginx и Mysql для filebeat и настраиваем глобальную конфигруацию и конфигурацию модулей
5. Проверяем логи в kibana
![Логи Nginx](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-7/images/nginx_access.png)
6. Настраиваем metricbeat и проверяем метрики
![Метрики Nginx](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-7/images/nginx_metrics.png)
7. Настраиваем heartbeat и смотрим статус в kibana
![Heartbeat](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-7/images/heartbeat.png)