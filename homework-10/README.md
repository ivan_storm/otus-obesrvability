1. Копируем docker-compose.yaml, который запустит opensearch + opensearch dashboard + data prepper + fluentbit
2. Добавляем необходимые файлы конфигураций fluentbit, log_pipeline и data prepper из configs. Логи собираем только по Nginx и Mysql
3. Запускаем docker compose и проверяем, что все контейнеры успешно запустились
4. Идём в Opensearch Dashboard и создаем новый index pattern website-logs
![Index Pattern](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-10/images/index_pattern.png)
5. Переходим на вкладку Discover, нам доступен сразу индекс website-logs, выбираем поле log
![Index Pattern](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-10/images/discover.png)
6. Пробуем погенерировать сообщения по веб-сайту, а также повыполнять запросы внутри БД mysql. В итоге видим, что логи записываются успешно
![Index Pattern](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-10/images/website-logs.png)