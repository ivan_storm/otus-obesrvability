1. Устанавливаем graylog-sidecar на ВМ с bookstack, добавляем конфигурацию как в файле sidecar.yaml и запускаем graylog-sidecar как сервис, проверяем появился ли наша ВМ в sidecars
![Добавленный graylog-sidecar](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-11/images/sidecars.png)
2. Устанавливаем Opensearch и graylog-server на ВМ mon-01
3. Создаем новый Input на основе Beats
![Добавление Input](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-11/images/graylog_inputs.png)
4. Создаем новую конфигурацию коллектора на основе filebeat и data prepper и применяем её к нашему graylog-sidecar
5. Проверяем, что логи прилетают в разделе Search
![Логи от sidecar](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-11/images/graylog_searchs.png)
6. Добавляем новые стримы для Nginx и Mysql
![Стримы](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-11/images/graylog_streams.png)
7. Логи от Nginx
![Логи Nginx](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-11/images/nginx_logs.png)
8. Логи от MySQL
![Логи MysQL](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-11/images/mysql_logs.png)