1. Устанавливаем на ВМ с bookstack telegraf и настраиваем его как в файле telegraf.conf с отправкой в influxdb системных метрик, метрик nginx, mysql
2. Настраиваем дополнительную конфигурацию для Nginx и размещаем ее в conf.d как status.conf:
```shell
server {
    listen 8080;
    server_name localhost;
    keepalive_timeout 0;

    access_log off;

    allow 127.0.0.1;
    allow 192.168.0.0/16;
    deny all;

    location /nginx_status {
        stub_status on;
    }
}
```
3. Устанавливаем на ВМ mon-01 influxdb, chronograf и kapacitor
4. Заходим на адрес ВМ по порту 8888 и попадаем в Chronograf, выполняем подключение к influxdb и kapacitor
5. Подключаем telegram для отправки в него сообщений:
![Подключение Telegram](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-6/images/telegram_connection.png)
6. Создаем сводный дашборд с важными для меня параметрами:
![Подключение Telegram](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-6/images/chronograf_app_dashboard.png)
7. Настраиваем алерты по ОЗУ и доступности MySQL:
8. Проверяем, что алерты прилетают в канал в телеграме
![Подключение Telegram](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-6/images/telegram_alerts.png)