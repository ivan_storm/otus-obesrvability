Использовались следующие манулы:
 - https://www.dmosk.ru/instruktions.php?object=grafana-loki
 - https://dzen.ru/a/ZHn5IKmq3Bz8JvWc

1. Устанавливаем на ВМ otus-vm-01 (где есть nginx + mysql) log-shapper promtail по мануалу 
2. На эту же ВМ устанавливаем golang и затем Grafana Loki (сборка из исходного кода на go) по мануалу 
3. Настраиваем loki в соответствии с конфигурацией loci-local-config.yaml
4. Настраиваем promtail на передачу логов nginx и mysql в Grafana Loki, используя конфигурацию promtail.yaml
5. Проверяем, что таргеты захватываются, зайдя по адресу http://192.168.62.22:9080/targets
![Таргеты Promtail](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-9/images/promtail_targets.png)
6. Идём в Grafana, которую устанавливали в предыдущих заданиях и добавляем новый источник - loki:
![Источник Loki в Grafana](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-9/images/loki_source.png)   
7. Создаём новый дашборд и называем его Logs, добавляем панели с табличными данными внутри на основе данных из loki:
![Выборка данных в дашборде](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-9/images/grafana_data.png)
8. Проверяем дашборд
![Готовый дашборд в Grafana](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-9/images/grafana_loki.png)