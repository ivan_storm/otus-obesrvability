1. Продолжаем работу с предыдущей инсталляцией на ВМ otus-vm-01, устанавливаем Logstash
2. В конфигурации logstash.yml включаем поддержку Dead letter queue (DLQ)
3. Добавляем отдельную конфигурацию под нашу задачу отправки и парсинга логов. Будем рассматривать Nginx 
4. Перенастраиваем filebeat на отправку логов в logstash:
```shell
# ---------------------------- Elasticsearch Output ----------------------------
#output.elasticsearch:
# Array of hosts to connect to.
#  hosts: ["192.168.62.22:9200"]
#  username: "elastic"
#  password: "=BKTfTvs4oXfJuA4ep0K"
#  ssl.verification_mode: none
#
output.logstash:
  hosts: ["192.168.62.22:5044"] 
```
5. Создаем новый data view с именем Web Site (в выборе должен быть наш индекс, указаный в конфиг. файле)
![Create a data view](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-8/images/create_dataview.png)
6. Проверяем на вкладке Discover, что логи прилетают
![Логи](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-8/images/web_logs.png)
7. Создаем ILM политику  и добавляем в конфигурацию website.conf в Output включение ILM. Через curl или Dev Tools добавляем:
![Создание ILM политики](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-8/images/ilm_policy.png)
8. Проверяем политику в веб-интерфейсе Kibana
![ILM политика в вебе](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-8/images/ilm_policy_in_web.png)
9. Устанавливаем Vector и применяем конфигурацию из файла vector.yaml
10. Проверяем, что Vector добавился и логи прилетают
![Nginx логи от Vector](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-8/images/nginx_logs_vector.png)