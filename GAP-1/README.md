В качестве операционной системы используется Debian 12 Bookworm. Характеристики ВМ - 4 Core, 8 GB Ram, 30 GB SSD
В этом разделе настроим базовую инфраструктуру необходимую для дальнейшей работы.
В качестве альтернативного DNS-сервера нужно прописать ip-адрес ВМ.

1. Настраиваем сеть, хостнейм и DNS:
```shell
su - # для перехода под root
#/etc/network/interfaces:
auto ens18
iface ens18 inet static
    address 192.168.60.11/24
    gateway 192.168.60.1

#/etc/resolv.conf
domain otuslab.local
nameserver 77.88.8.8
nameserver 192.168.60.11

#/etc/hostname
otus-vm

#/etc/hosts
127.0.0.1       otus-vm
192.168.60.11    otus-vm
```

2. Перезагружаем ВМ командой reboot

3. Настраиваем репозиторий, устанавливаем необходимые пакеты, создаем пользователя и добавляем его в админы:
```shell
echo 'deb https://mirror.yandex.ru/debian bookworm main contrib' > /etc/apt/sources.list
apt update && apt install -y sudo git curl wget net-tools dnsutils telnet tmux easy-rsa apache2-utils
adduser otusadmin
<пароль>
<подтверждаем пароль>
usermod -aG sudo otusadmin
# заходим под нашим пользователем
su - otusadmin
<пароль>
```

4. Устанавливаем Docker и Docker Compose:
```shell
sudo curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh ./get-docker.sh
# Проверяем версии Docker и Docker Compose
docker --version
Docker version 26.0.1, build d260a54
docker compose version
Docker Compose version v2.26.1
# Не забываем добавить пользователя в группу docker
sudo usermod -aG docker otusadmin
```

5. Создаем каталоги для размещения композов и файлов конфигураций:
```shell
sudo mkdir -p /opt/composes/observability
sudo mkdir /opt/composes/wordpress
sudo mkdir /opt/ca
```

6. Устанавливаем Bind9 и настраиваем DNS-сервер
Установка BIND и всех необходимых пакетов производится одной командой:
```shell
sudo apt update && sudo apt install -y bind9
```
Несмотря на то, что имя пакета bind9 установленная служба имеет название named.

Затем откроем файл __/etc/bind/named.conf.options__ и после строки:
__directory "/var/cache/bind";__
```shell
 listen-on port 53 { 127.0.0.1; 192.168.60.11; };
 allow-query { 127.0.0.0/8; 192.168.0.0/16; };
 allow-recursion { 127.0.0.0/8; 192.168.0.0/16; };
 allow-transfer { none; };
 forwarders { 77.88.8.8; };
```

Редактируем также и следующую опцию:
```shell
dnssec-validation no;
 listen-on-v6 { none; };
 ```

Добавляем в файл __/etc/bind9/named.conf.local__ описания прямой и обратной зон:
```shell
zone "otuslab.local" {
     type master;
     file "/var/lib/bind/db.otuslab.local"; 
};

zone "60.168.192.in-addr.arpa" {
     type master;
     file "/var/lib/bind/db.192.168.60";
};
```
Далее заполняем файлы прямой зоны:
```shell
touch /var/lib/bind/db.otuslab.local
$TTL 3600 ;
@ IN SOA dns.otuslab.local. root.otuslab.local. (
         2024042001 ; Serial
         600 ; Refresh
         3600 ; Retry
         1w ; Expire
         360 ) ; Negative Cache TTL
@ IN NS dns.otuslab-local.

@       IN A 192.168.60.11
dns     IN A 192.168.60.11
otus-vm IN A 192.168.60.11
prometheus IN CNAME otus-vm
wordpress  IN CNAME otus-vm
zabbix     IN CNAME otus-vm
grafana    IN CNAME otus-vm
```

Заполняем файл обратной зоны:
```shell
touch /var/lib/bind/db.192.168.60
$TTL 3600 ;
@ IN SOA dns.otuslab.local. root.otuslab.local. (
         2024042001 ; Serial
         600 ; Refresh
         3600 ; Retry
         1w ; Expire
         360 ) ; Negative Cache TTL

@ IN NS dns.otuslab-local.

11 IN PTR dns.interface31.lab.
11 IN PTR prometheus.otuslab.local.
11 IN PTR grafana.otuslab.local.
11 IN PTR zabbix.otuslab.local.
11 IN PTR wordpress.otuslab.local.
```

После того, как вы выполнили все настройки, а также создали и настроили файлы зон можно проверить функционирование нашего сервера. Сначала проверим конфигурацию на ошибки:
```shell
named-checkconf
```
Затем проверим зоны, для этого указываем их наименования и пути к файлам данных:
```shell
named-checkzone otuslab.local /var/lib/bind/db.otuslab.local
zone otuslab.local/IN: loaded serial 2024042001
OK

named-checkzone 60.168.192.in-addr.arpa /var/lib/bind/db.192.168.60
/var/lib/bind/db.192.168.60:1: no TTL specified; using SOA MINTTL instead
zone 60.168.192.in-addr.arpa/IN: loaded serial 2024042001
OK
```

Если ошибок не зафиксировано, то перезапускаем службу и проверяем ее работу:
```shell
systemctl restart named
systemctl status named
# проверяем работу DNS (сначала прямую запись, потом обратную)
nslookup prometheus.otuslab.local 192.168.60.11
nslookup 192.168.60.11 192.168.60.11
```

7. Создаем центр сертификации с помощью Easy-RSA
Скопируем рабочую директорию Easy-RSA в текущую директорию и изменим права владения:
```shell
cd /opt/ca/
sudo cp -r /usr/share/easy-rsa .
sudo chown -R otusadmin:otusadmin easy-rsa/
sudo chmod -R 700 easy-rsa/
```
Настроим Easy-RSA, раскомментировав и изменив следующие строки в конфигурационном файле __/easy-rsa/vars.example__:
```shell
sсt_var EASYRSA_REQ_COUNTRY     "RU"
set_var EASYRSA_REQ_PROVINCE    "Niznhiy Novgorod"
set_var EASYRSA_REQ_CITY        "Niznhiy Novgorod"
set_var EASYRSA_REQ_ORG         "OTUS LAB"
set_var EASYRSA_REQ_EMAIL       "ivan_mitkin.zvl@mail.ru"
set_var EASYRSA_REQ_OU          "LLC"
set_var EASYRSA_ALGO            ec
set_var EASYRSA_DIGEST          "sha512"
```

Применим конфигурацию и создадим инфраструктуру открытых ключей (Public Key Infrastructure):
```shell
cd easy-rsa
mv vars.example vars
./easyrsa init-pki
```

В результате будет создана директория __/easy-rsa/pki__, в которой будут храниться все создаваемые в дальнейшем ключи.

Теперь сгенерируем ключи удостоверяющего центра:
```shell
./easyrsa build-ca
```
Система запросит у нас ввести пароль для приватного ключа ca и имя нового ca:
```shell
* Notice:

  init-pki complete; you may now create a CA or requests.

  Your newly created PKI dir is:
  * /opt/ca/easy-rsa/pki

otusadmin@otus-vm:/opt/ca/easy-rsa$ ./easyrsa build-ca
* Notice:
Using Easy-RSA configuration from: /opt/ca/easy-rsa/vars

* WARNING:

  Move your vars file to your PKI folder, where it is safe!

* Notice:
Using SSL: openssl OpenSSL 3.0.11 19 Sep 2023 (Library: OpenSSL 3.0.11 19 Sep 2023)


Enter New CA Key Passphrase: 
Re-Enter New CA Key Passphrase: 
Using configuration from /opt/ca/easy-rsa/pki/42df2be3/temp.0740eb79
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Common Name (eg: your user, host, or server name) [Easy-RSA CA]:OTUSLAB-CA

* Notice:

CA creation complete and you may now import and sign cert requests.
Your new CA certificate file for publishing is at:
/opt/ca/easy-rsa/pki/ca.crt
```
В результате программа создаст два файла:
__/opt/ca/easy-rsa/pki/ca.crt__ - открытый ключ;
__/opt/ca/easy-rsa/pki/private/ca.key__ - закрытый ключ.

Открытый ключ можно сводобно передавать на другие vm, а закрытый необходимо хранить в строжайшем секрете.

Чтобы добавить сертификат в доверенные корневые необходимо скопировать __ca.crt__ в __/usr/local/share/ca-certificates/__
```shell
cp easy-rsa/pki/ca.crt /usr/local/share/ca-certificates/
update-ca-certificates
# результат
Updating certificates in /etc/ssl/certs...
rehash: warning: skipping ca-certificates.crt,it does not contain exactly one certificate or CRL
1 added, 0 removed; done.
Running hooks in /etc/ca-certificates/update.d...
done.
```

Для выпуска и подписания новой пары ключ-сертификат для сервера с доменным именем __wordpress.otuslab.local__, необходимо выполнить следующие действия:
```shell
cd easy-rsa
./easyrsa gen-req wordpress.otuslab.local nopass
# вывод такой
* Notice:
Using Easy-RSA configuration from: /opt/ca/easy-rsa/vars

* WARNING:

  Move your vars file to your PKI folder, where it is safe!

* Notice:
Using SSL: openssl OpenSSL 3.0.11 19 Sep 2023 (Library: OpenSSL 3.0.11 19 Sep 2023)

-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Common Name (eg: your user, host, or server name) [wordpress.otuslab.local]:
* Notice:

Keypair and certificate request completed. Your files are:
req: /opt/ca/easy-rsa/pki/reqs/wordpress.otuslab.local.req
key: /opt/ca/easy-rsa/pki/private/wordpress.otuslab.local.key

# подписываем сертификат корневым 
./easyrsa sign-req server wordpress.otuslab.local
* Notice:
Using Easy-RSA configuration from: /opt/ca/easy-rsa/vars

* WARNING:

  Move your vars file to your PKI folder, where it is safe!

* Notice:
Using SSL: openssl OpenSSL 3.0.11 19 Sep 2023 (Library: OpenSSL 3.0.11 19 Sep 2023)


You are about to sign the following certificate.
Please check over the details shown below for accuracy. Note that this request
has not been cryptographically verified. Please be sure it came from a trusted
source or that you have verified the request checksum with the sender.

Request subject, to be signed as a server certificate for 825 days:

subject=
    commonName                = wordpress.otuslab.local


Type the word 'yes' to continue, or any other input to abort.
  Confirm request details: yes

Using configuration from /opt/ca/easy-rsa/pki/9c391bba/temp.0bd5a97c
Enter pass phrase for /opt/ca/easy-rsa/pki/private/ca.key:
Check that the request matches the signature
Signature ok
The Subject's Distinguished Name is as follows
commonName            :ASN.1 12:'wordpress.otuslab.local'
Certificate is to be certified until Aug  4 09:53:12 2026 GMT (825 days)

Write out database with 1 new entries
Database updated

* Notice:
Certificate created at: /opt/ca/easy-rsa/pki/issued/wordpress.otuslab.local.crt
```
В результате мы получим два файла:
__easy-rsa/pki/issued/wordpress.otuslab.local.crt__ - подписанный сертификат;
__easy-rsa/pki/private/wordpress.otuslab.local.key__ - приватный ключ.

Чтобы посмотреть валидность сертификата на ВМ, а также его содержимое нужно ввести следующие команды:
Проверка валидности сертификата (доверие сертификату):
```shell
openssl verify wordpress.otuslab.local.crt
wordpress.otuslab.local.crt: OK
```
Читаем содержимое сертификата:
```shell
openssl x509 -in wordpress.otuslab.local.crt -text
```

Сертификаты для других сервисов делаются аналогичным образом. Можно сделать один общий сертификат и прикрутить его к прокси-серверу.

8. Создаем и переходим в каталог с нашей CMS __/opt/composes/wordpress__, в котором будет докер-композ файл с вордпрессом и конфигурация Nginx
```shell
mkdir -p /opt/composes/wordpress
cd /opt/composes/wordpress
nano docker-compose.yaml
```

```yaml
version: '3'

services:
  db:
    image: mysql:8.0
    container_name: db
    restart: unless-stopped
    env_file: .env
    environment:
      - MYSQL_DATABASE=wordpress
    volumes:
      - dbdata:/var/lib/mysql
    command: '--default-authentication-plugin=mysql_native_password'
    dns:
      - 192.168.61.10
    ports:
      - 3306:3306
    networks:
      - app-network

  wordpress:
    depends_on:
      - db
    image: wordpress:5.1.1-fpm-alpine
    container_name: wordpress
    restart: unless-stopped
    dns:
      - 192.168.61.10
    env_file: .env
    environment:
      - WORDPRESS_DB_HOST=db:3306
      - WORDPRESS_DB_USER=$MYSQL_USER
      - WORDPRESS_DB_PASSWORD=$MYSQL_PASSWORD
      - WORDPRESS_DB_NAME=wordpress
    volumes:
      - wordpress:/var/www/html
    networks:
      - app-network

  webserver:
    depends_on:
      - wordpress
    image: nginx:1.15.12-alpine
    container_name: webserver
    restart: unless-stopped
    dns:
      - 192.168.61.10
    ports:
      - "80:80"
      - "443:443"
      - "8080:8080"
    volumes:
      - wordpress:/var/www/html
      - ./nginx-conf:/etc/nginx/conf.d
      - ./certs:/etc/nginx/ssl/
      - ./.htpasswd:/etc/nginx/.htpasswd
    networks:
      - app-network

volumes:
  wordpress:
  dbdata:

networks:
  app-network:
    driver: bridge
```

Также пишем следующую конфигурацию Nginx, размещенную в __nginx-conf/nginx.conf__:
```shell
server {
    listen 80;
    server_name wordpress.otuslab.local;

    auth_basic "Login to continue";
    auth_basic_user_file /etc/nginx/.htpasswd;

    location /prometheus/ {
       proxy_pass http://wordpress.otuslab.local:9090/;
       proxy_set_header Host $host;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       proxy_set_header X-Forwarded-Proto $scheme;
       proxy_redirect http:// https://;
    }

    location /blackbox-exporter/ {
        proxy_pass http://wordpress.otuslab.local:9115/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}

server {
        server_name wordpress.otuslab.local www.wordpress.otuslab.local;

        ssl on;
        ssl_session_timeout 24h;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
        listen 443 ssl http2;# proxy_protocol;

        ssl_certificate /etc/nginx/ssl/wordpress.otuslab.local.crt;
        ssl_certificate_key /etc/nginx/ssl/wordpress.otuslab.local.key;

        index index.php index.html index.htm;

        root /var/www/html;
 
        auth_basic "Login to continue";
        auth_basic_user_file /etc/nginx/.htpasswd;

        location ~ /.well-known/acme-challenge {
                allow all;
                root /var/www/html;
        }

        location / {
                try_files $uri $uri/ /index.php$is_args$args;
        }

        location ~ \.php$ {
                try_files $uri =404;
                fastcgi_split_path_info ^(.+\.php)(/.+)$;
                fastcgi_pass wordpress:9000;
                fastcgi_index index.php;
                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_param PATH_INFO $fastcgi_path_info;
        }

        location ~ /\.ht {
                deny all;
        }
 
        location = /favicon.ico {
                log_not_found off; access_log off;
        }

        location = /robots.txt {
                log_not_found off; access_log off; allow all;
        }

        location ~* \.(css|gif|ico|jpeg|jpg|js|png)$ {
                expires max;
                log_not_found off;
        }
      
        location /node-exporter {
                proxy_pass http://wordpress.otuslab.local:9100;
                proxy_http_version 1.1;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }


        location /mysql-exporter {
                proxy_pass http://wordpress.otuslab.local:9104;
                proxy_http_version 1.1;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

        location /nginx-exporter {
                proxy_pass http://wordpress.otuslab.local:9113;
                proxy_http_version 1.1;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

        location /php-fpm-exporter {
                proxy_pass http://wordpress.otuslab.local:9253;
                proxy_http_version 1.1;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
}
```

Создаем каталог __certs__ и копируем наши созданные ранее сертификаты:
```shell
mkdir certs
cp /opt/ca/easy-rsa/pki/issued/wordpress.otuslab.local.crt certs/
cp /opt/ca/easy-rsa/pki/issued/wordpress.otuslab.local.key certs/
```

Также нам нужно сгенерировать пароль для учетки, чтобы проходить базовую аутентификацию:
```shell
/opt/composes/wordpress# htpasswd -c .htpasswd otusadmin
New password: 
Re-type new password: 
Adding password for user otusadmin
```

И запускаем наш докер композ:
```shell
# запускаем
docker compose up -d
# проверяем, что все контейнеры в состоянии Up
docker compose ps
```

9. Переходим к мониторингу. Создаем и переходим в каталог __/opt/composes/prometheus__ и создаем docker-compose.yaml, в котором будут содержаться все нужные нам сервисы и добавляем конфигурационные файлы:
```shell
mkdir -p /opt/composes/prometheus
cd /opt/composes/prometheus
nano docker-compose.yaml
```

```yaml
version: '3.8'

networks:
  monitoring:
    driver: bridge

volumes:
  prometheus_data: {}

services:
  node-exporter:
    image: prom/node-exporter:latest
    container_name: node-exporter
    restart: unless-stopped
    dns:
      - 192.168.61.10
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command:
      - '--path.procfs=/host/proc'
      - '--path.rootfs=/rootfs'
      - '--path.sysfs=/host/sys'
      - '--collector.filesystem.mount-points-exclude=^/(sys|proc|dev|host|etc)($$|/)'
      - '--web.telemetry-path=/node-exporter/metrics'
    ports:
      - 9100:9100
    networks:
      - monitoring
  
  mysql-exporter:
    image: prom/mysqld-exporter:main
    container_name: mysql-exporter
    command:
      - '--config.my-cnf=/cfg/.my.cnf'
      - '--web.telemetry-path=/mysql-exporter/metrics'
    restart: unless-stopped
    dns:
      - 192.168.61.10
    volumes:
      - ./.my.cnf:/cfg/.my.cnf
    environment:
      - DATA_SOURCE_NAME=exporter:P@ssw0rd@(localhost:3306)/db
    ports:
      - 9104:9104
    networks:
      - monitoring

  nginx-exporter:
    image: nginx/nginx-prometheus-exporter:1.1.0
    container_name: nginx-exporter
    command:
     - '--web.telemetry-path=/nginx-exporter/metrics'
    restart: unless-stopped
    dns:
      - 192.168.61.10
    ports:
      - 9113:9113
    networks:
      - monitoring

  prometheus:
    image: prom/prometheus:latest
    container_name: prometheus
    restart: unless-stopped
    dns:
      - 192.168.61.10
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml
      - prometheus_data:/prometheus
    command:
      - '--config.file=/etc/prometheus/prometheus.yml'
      - '--web.console.libraries=/etc/prometheus/console_libraries'
      - '--web.console.templates=/etc/prometheus/consoles'
      - '--web.enable-lifecycle'
      - '--web.external-url=http://wordpress.otuslab.local/prometheus/'
      - '--web.route-prefix=/'
    ports:
      - 9090:9090
    networks:
      - monitoring

  php-fpm-exporter:
    image: hipages/php-fpm_exporter
    container_name: php-fpm-exporter
    command:
      - '--web.telemetry-path=/php-fpm-exporter/metrics'
    restart: unless-stopped
    dns:
      - 192.168.61.10
    ports:
      - 9253:9253
    networks:
      - monitoring

  blackbox-exporter:
    image: prom/blackbox-exporter
    container_name: blackbox
    command:
      - '--config.file=/config/blackbox.yml'
      - '--web.external-url=https://wordpress.otuslab.local:9115/blackbox-exporter/'
      - '--web.route-prefix=/'
    restart: unless-stopped
    dns:
      - 192.168.61.10
    volumes:
      - ./blackbox.yml:/config/blackbox.yml
    ports:
      - 9115:9115
    networks:
      - monitoring
```
Добавим конфигурацию __prometheus.yml__:
```yaml
global:
  scrape_interval: 30s

scrape_configs:
  - job_name: 'prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['wordpress.otuslab.local:9090']

  - job_name: 'db-mysql-wordpress'
    scrape_interval: 5s
    metrics_path: /mysql-exporter/metrics
    static_configs:
      - targets: ['wordpress.otuslab.local:9104']

  - job_name: 'otus-vm'
    scrape_interval: 5s
    metrics_path: /node-exporter/metrics
    static_configs:
      - targets: ['wordpress.otuslab.local:9100']

  - job_name: 'nginx'
    scrape_interval: 5s
    metrics_path: /nginx-exporter/metrics
    static_configs:
      - targets: ['wordpress.otuslab.local:9113']

  - job_name: 'php-fpm'
    scrape_interval: 5s
    metrics_path: /php-fpm-exporter/metrics
    static_configs:
      - targets: ['wordpress.otuslab.local:9253']

  - job_name: 'get-status-wordpress'
    metrics_path: /probe
    params:
      module: [http_2xx]
    scrape_interval: 5s
    static_configs:
      - targets:
        - https://wordpress.otuslab.local
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: wordpress.otuslab.local:9115
```

Добавим конфигурацию __blackbox.yml__:
```yaml
modules:
  http_2xx:
    prober: http
    timeout: 5s
    http:
      valid_status_codes: []
      method: GET
      preferred_ip_protocol: ip4
      fail_if_ssl: false
      fail_if_not_ssl: false
      tls_config:
        insecure_skip_verify: true
```

Для MySQL необходимо создать пользователя в БД и добавить файл с данными __.my.cnf__:
```yaml
[client]
user = exporter
password = P@ssw0rd
```
Подключаемся к БД внутри Docker контейнера:
```sql
docker exec -it db bash
mysql -u root -p
<пароль>
CREATE USER 'exporter'@'localhost' IDENTIFIED BY 'P@ssw0rd' WITH MAX_USER_CONNECTIONS 3;
GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'exporter'@'localhost';
```

Запускаем docker compose:
```shell
# запускаем
docker compose up -d
# проверяем
docker compose ps
```

10. Далее можем постучаться на следующие страницы:
  - Prometheus - http://wordpress.otuslab.local/prometheus
  - Blackbox - http://wordpress.otuslab.local/blackbox-exporter
  - Node-Exporter - https://wordpress.otuslab.local/node-exporter
  - Nginx-Exporter - https://wordpress.otuslab.local/nginx-exporter
  - PHP-FPM-Exporter - https://wordpress.otuslab.local/php-fpm-exporter
  - MySQL-Exporter - https://wordpress.otuslab.local/mysql-exporter

11. Вот так выглядит наша CMS Wordpress и Prometheus Targets:
![SSL + Аутентификация](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/GAP-1/images/base_auth.png?ref_type=heads)
![CMS Wordpress](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/GAP-1/images/wordpress.png?ref_type=heads)
![Prometheus Targets](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/GAP-1/images/prometheus_targets.png?ref_type=heads)

12. Отказоустойчивость Prometheus, хранилища метрик для Prometheus
Добавляем в docker-compose.yaml Victoria Metrics с периодом хранения метрик в 2 недели:
```yaml
victoria_metrics:
    image: victoriametrics/victoria-metrics
    ports:
      - 8428:8428
    volumes:
      - ./vm_data:/victoria-metrics-data
    command: 
      - '--retentionPeriod=14d'
```

Добавляем хранилище в конфигурацию Prometheus, а также задачу по сбору метрик с Victoria Metrics:
```yaml
global:
  scrape_interval: 30s
  external_labels:
    monitor: 'site:prod'

remote_write:
  - url: http://wordpress.otuslab.local:8428/api/v1/write
    queue_config:
      max_samples_per_send: 10000
      capacity: 20000
      max_shards: 30

  - job_name: 'victoria-metrics'
    scrape_interval: 5s
    static_configs:
      - targets: ['wordpress.otuslab.local:8428']
```

Перезапускаем наш Docker Compose:
```shell
docker compose up -d
docker compose ps
```

![Prometheus Victoria Metrics](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/GAP-1/images/prometheus_victoria_metrics.png?ref_type=heads)
![Victoria Metrics tsdb](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/GAP-1/images/victoria_metrics_tsdb.png?ref_type=heads)
![Victoria Metrics UI](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/GAP-1/images/victoria_metrics_vmui.png?ref_type=heads)

13. Добавление Alertmanager с отправкой сообщений в телеграм канал

Добавляем настройки Alermanager в конфигурацию prometheus.yml:
```yaml
rule_files:
  - rules.yaml

alerting:
  alertmanagers:
  - static_configs:
    - targets: ['wordpress.otuslab.local:9093']
```

Добавляем Alertmanager в конец файла docker-compose.yaml:
```yaml
  alertmanager:
    image: prom/alertmanager
    container_name: alertmanager
    restart: unless-stopped
    dns:
      - 192.168.61.10
    volumes:
      - ./alertmanager:/alertmanager/data
      - ./alertmanager/alertmanager.yaml:/etc/alertmanager/
    command:
      - '--config.file=/etc/alertmanager/alertmanager.yaml'
    ports:
      - 9093:9093

```

Чтобы AlertManager отправлял оповещения в Telegram, создадим Telegram-бота. API-ключ для Telegram можно получить у @BotFather. Для этого введем команду, которая создает и регистрирует нового бота — /newbot и дадим ему имя:



Добавляем конфигурацию самого alertmanager - alertmanager.yaml:
```yaml
global:
 resolve_timeout: 5m
 telegram_api_url: "https://api.telegram.org"

templates:
  - '/etc/alertmanager/*.tmpl'

receivers:
 - name: blackhole
 - name: telegram-test
   telegram_configs:
    - chat_id: "ID вашего чата без кавычек"
      bot_token: "Ваш токен без кавычек"
      api_url: "https://api.telegram.org"
      send_resolved: true
      parse_mode: HTML
      message: '{{ template "telegram.default" . }}'


route:
 group_by: ['ds_id'] # Алерты группируются по UUID кластера.
 group_wait: 15s
 group_interval: 30s
 repeat_interval: 12h
 receiver: telegram-test
 routes:
  - receiver: telegram-test
    continue: true
    matchers:
     - severity="critical"
  - receiver: blackhole
    matchers:
     - alertname="Watchdog"
```


