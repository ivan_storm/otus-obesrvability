Виртуальные машины:
- mon-01 - 192.168.62.30 - стэк GAP + Node Exporter
- mon-02 - 192.168.62.22 - Node Exporter

Картинки размещены в каталоге images.

1. На mon-01 создаем файл docker-compose.yaml с сервисами Grafana, Prometheus, Node Exporter, MySQL Exporter, BlackBox, AlertManager и AlertManager Bot (бот в итоге не использовался), а также файл .env с необходимыми переменными
2. Добавляем конфигурации Prometheus, BlackBox, MySQL Exporter и Alertmanager
3. На mon-02 устанавливаем prometheus-node-exporter и запускаем сервис
4. Логинимся в Grafana по ip-адресу и порту 3000, меняем пароль на свой
5. Создаем папки app и infra
6. Подключаем Prometheus к Grafana
7. Импортируем шаблон Node Exporter и переименовываем его в Netlab Infrastructure и проверяем отображение данных по нодам (должно быть две ноды mon-01 и mon-02)
8. В той же папке создаем новый дашборд Infra Summary, в которой будет сводная по важным параметрам. Данный дашборд будет как drilldown:
  * Из первого шаблона берем панели CPU Busy, Uptime, Root FS Used и RAM Used, делаем из них Create library panel
  * Добавляем все эти панели на наш новый дашборд
  * В свойствах дашборда добавляем три переменных:
  * datasource со значениями default,prometheus;
  * job со значением node-exporters;
  * node со значениями mon-01:9100,mon-02:9100
  * В свойствах переменной node ставим флаги Multi-value И Include all options
  * В каждом графике меняем в запросе __=__ на __=~__, меняем значение Legend на __{{instance}}__ и сохраняем панель
  * Добавляем пустой график, вносим запрос __up{instance=~"$node"}__, меняем значение Legend на __{{instance}}__, меняем значение визуализации на Stat, сопоставляем значение 0 как Down и 1 как Up и сохраняем панель
  * График CPU почему-то не отображает несколько нод, не обращаем внимания на это
9. Добавляем к графику Server Status линки в формате - http://192.168.62.30:3000/d/rYdddlPWk/netlab-infrastructure?orgId=1&refresh=1m&var-node=${__field.labels.instance}
10. Проверяем, что по нажатию на ноду на графике открывается новая вкладка с графиком по нажатой ноде
11. В папку app импортируем шаблоны Bookstack, переименовываем его в Bookstack, шаблон MySQL Prometheus как Bookstack MySQL и Blackbox как Доступность Bookstack
12. В дашборде Доступность Bookstack проверяем по https доступен ли адрес https://wiki.netlab.local/login, в графике Bookstack добавляем информацию по содержимому сайта (это книги, пользователи, сколько отредактировано раз и т.д.), в MySQL по базе данных
13. Создаем общий Dashboard Bookstack App и добавляем в него графики из доступности и по книгам
14. Создаем бота в телеграме через BotFather, делаем чат для уведомлений и подключаем на основе этих данных Telegram к Grafana через Contact points
15. Создаем правило уведомления на основе графика Server Status, если статус 0, то отправляем уведомление в телеграм о тои, что сервер в Down. По восстановлению обратное сообщение