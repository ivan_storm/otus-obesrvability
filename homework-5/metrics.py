#!/usr/bin/python3
import json
import random

def generate_metrics(metric_names):
    metrics = []
    for name in metric_names:
        metric_value = random.randint(0, 100)
        metrics.append({
            "name": name,
            "value": metric_value
        })
    return {"data": metrics}

if __name__ == "__main__":
    METRIC_NAMES = ["metric1", "metric2", "metric3"]
    result = generate_metrics(METRIC_NAMES)

    # Выводим результат в формате JSON
    print(json.dumps(result, indent=2))