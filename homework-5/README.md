Выполняем следующие действия
1. Устанавливаем Zabbix Agent на хост otus-vm-01
2. Создаем каталог scripts и размещаем в нем Python скрипт metrics.py
```python
#!/usr/bin/python3
import json
import random

def generate_metrics(metric_names):
    metrics = []
    for name in metric_names:
        metric_value = random.randint(0, 100)
        metrics.append({
            "name": name,
            "value": metric_value
        })
    return {"data": metrics}

if __name__ == "__main__":
    METRIC_NAMES = ["metric1", "metric2", "metric3"]
    result = generate_metrics(METRIC_NAMES)

    # Выводим результат в формате JSON
    print(json.dumps(result, indent=2))

```
3. Меняем конфигурацию zabbix_agentd.conf в соответствии с файлом в репозитории
4. Перезапускаем службу Zabbix агента
5. Добавляем хост на мониторинг как Zabbix Agent
6. В Zabbix добавляем к хосту новый элемент данных с параметрами как на скриншоте
![Элемент данных](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-5/images/data_element.png)
7. Добавляем к хосту новое правило автообнаружения с параметрами как на скриншоте
![Правило автообнаружения](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-5/images/discover_rule.png)
8. Добавляем LLD макросы как на скриншоте
![LLD макросы](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-5/images/lld_macros.png)
9. Переходим на вкладку прототип данных и создаем новый прототип элементов данных, настраиваем как на скриншоте, в том числе предобработку
![Прототип элементов данных](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-5/images/prototype_data.png)
![Предобработка JSON файла](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-5/images/predobrabotka.png)
10. Переходим на вкладку прототипы триггеров и создаем новый прототип триггеров, настраиваем как на скриншоте и вешаем тэг telegram со значением alarm
![Триггер](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-5/images/trigger.png)
11. Используем токен ранее созданного бота для активации Telegram оповещений
12. Пользователю admin добавляем оповещения в Telegram в необходимый канал как на скриншоте
![Настройки уведомлений пользователю](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-5/images/user_notify.png)
13. Создаем действие для триггера, в рамках которого будут отправляться уведомления в телеграм. Настройки как на скриншоте
![Настройки отправки уведомления](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-5/images/send_notify.png)
14. Проверяем, что графики для всех метрик строятся
![График 1-й метрики](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-5/images/metric_1.png)
![График 2-й метрики](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-5/images/metric_2.png)
![График 3-й метрики](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-5/images/metric_3.png)
15. Проверяем, что триггеры срабатывают корректно и уведомления в телеграм приходят
![Уведомления в Telegram](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/homework-5/images/telegram_notify.png)