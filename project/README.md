### Итоговый проект по курсу OTUS
#### Мониторинг и логирование инфраструктуры лаборатории для тестов сетевого оборудования
#### Что имеем?
Есть небольшая инфраструктура, необходимая для проведения тестов сетевого оборудования. На текущий момент инфраструктура не мониторится, что иногда приводит к долгому поиску проблем, а также к задержке проведения выполнения тестов. Инфраструктура также не логируется никаким образом, что тоже приводит к затруднениям в поисках того, кто, где и когда что-то сделал.

Инфраструктура состоит из следующих устройств:
- Маршрутизатор Cisco ISR 4431;
- Коммутатор ядра Cisco 3560;
- Коммутатор доступа Cisco 3560;
- Сервер с гипервизором ESXi;
- Сервер с гипервизором Proxmox VE;
- Мини-пк с развернутым сервером VPN;
- Тестовый коммутатор Huawei 5731;
- Тестовый коммутатор B4com;

Виртуальная инфраструктура по большей части развернута в VMware ESXi с оболочкой VSphere (VCenter) и состоит из следующих ВМ:
- Windows AD;
- Windows DHCP;
- Windows CA;
- Windows IIS;
- Linux Zabbix;
- Linux GAP;
- Linux Wiki;
- VCenter;

#### Цели
1. Настроить систему мониторинга инфраструктуры для тестирования сетевого оборудования с уведомлениями и оповещениями о критичных и важных событиях;
2. Настроить логирование важных для отслеживания объектов;

#### Задачи
1. Развернуть сервер Zabbix и настроить мониторинг сетевых устройств:
   - Мониторинг оборудования производить по SNMPv3;
   - Разделить сетевое оборудование на главное и тестовое;
   - Настроить триггеры и уведомления о падении критичных интерфейсов, а также высокой загрузке CPU и RAM в канал Telegram;
2. Развернуть сервер с Grafana, Prometheus, Alert Manager, InfluxDB для мониторинга серверов и виртуальной инфраструктуры:
   - Поставить на мониторинг VMware VSphere с использованием Telegraf;
   - Поставить на мониторинг Proxmox VE с использованием встроенного агента;
   - Поставить на мониторинг VPN-сервер для мониторинга подключений;
   - Поставить на мониторинг отдельно Microsoft инфраструтуру;
   - Настроить отправку уведомлений в Telegram в случае падения устройств и критичных ВМ;
3. Поставить на мониторинг веб-сайт с вики - Bookstack, мониторить необходимые данные
4. Настроить логирование сетевых устройств с использованием Grafana Loki и Promtail

#### Постановка на мониторинг сетевых устройств с помощью Zabbix
1. Выписываем сертификат для Zabbix сервера и запускаем с помощью docker compose контейнеры с Zabbix и MySQL
2. Прикручиваем __LDAP аутентификацию__ к Zabbix, создаем своего пользователя cо случайным паролем, выдаем админскую роль, проверяем вход под доменной УЗ
![Пользователи Zabbix](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/zabbix_ldap_user.png)
3. Настраиваем все сетевые устройства на __SNMPv3__. Все конфигурации SNMPv3 лежат в __configs/snmp_devices.txt__
4. Создаем две группы узлов сети - __Main Network Devices__ и __Test Network Devices__
5. Добавляем все сетевые устройства в Zabbix по __SNMPv3__, применяем подходящие шаблоны и добавляем в соответствующие группы
![Сетевое оборудование](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/zabbix_netdevices.png)
6. Настраиваем период обслуживания для тестовой группы устройств на каждый четверг __с 12:00 до 15:00__
![Период обслуживания](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/zabbix_thursday_services.png)
7. Включаем способ оповещения - Telegram и указывем токен созданного бота, проверяем, что всё ок
![Проверка отправки сообщений в Telegram](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/telegram_connection.png)
![Проверка отправки сообщений в Telegram](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/test_notify_zabbix_telegram.png)
8. Создаем группу пользователей __Telegram Notifications__ и добавляем туда админа и свою доменную УЗ, одной учеткой записи в оповещениях указываем тип Telegram и ID канала Telegram
![Настройка уведомлений пользователю в Telegram](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/zabbix_telegram_notifications.png)
9. Создаем действия по триггерам, которые будут отправлять уведомления в случае падения интерфейсов с физическим оборудованием, а также в случае ICMP-потерь и высокой загрузки CPU
![Действия триггеров](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/triggers_alerts.png)
10. Проверяем, что уведомления прилетают в телеграм. Для этого гасим один интерфейс в портченнеле и ждем уведомление, затем поднимаем интерфейс и смотрим уведомление о восстановлении
![Уведомления в телеграм](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/telegram_notifications_1.png)
11. Создадим карту сети, которая покажет нам общую схему лабораторного стенда
![Карта сети](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/network_map.png)
12. Создадим дашборд для основных сетевых устройств инфраструктуры. Поделим на 4 страницы: 
- на первой будут данные по железу, его доступности, загрузке CPU, RAM и времени работы
![Hardware Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/zabbix_hardware_dashboard.png)
- на второй будут данные по загрузке сетевых интерфейсов WAN граничного маршрутизатора, а также загрузки сетевых интерфейсов коммутатора ядра в сторонугипервизора, VPN-сервера и граничного маршрутизатора
![Interfaces Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/zabbix_interfaces_dashboard.png)
- на третьей будут данные по проблемам
![Problems Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/zabbix_problems_dashboard.png)
- на четвертой будет карта сети
![Network Map Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/zabbix_network_map.png)

#### Постановка на мониторинг VSphere и Proxmox VE
1. Использовать для мониторинга будем стэк __GAP__ + __Telegraf с Influxdb__ 
2. Используем ранее созданный docker compose для поднятия стэка GAP, на ВМ ставим отдельно Nginx и настраиваем проксирование __grafana.netlab.local__ на 3000 порт сервера с Grafana (конфигурация Nginx - __configs/grafana.conf__)
3. Локально устанавливаем __telegraf__ с __influxdb__. Создаем базу vmware и применяем конфигурацию telegraf для VSphere (__configs/vsphere-stats.conf__), перезапускаем сервис
4. Переходим в __Grafana__, создаем папку __VMware__ и импортируем дашборды (dashboards), которые будут отображать общую работу VCenter, хост ESXi, Хранилища и сводку по виртуальным машинам. Получаем следующие картинки:
- ![VSphere Overview](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/vsphere_overview_dashboard.png)
- ![VSphere Hosts](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/esxi_dashboard.png)
- ![VSphere Datastore](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/vsphere_datastore.png)
- ![VSphere Virtual Machines](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/vsphere_virtual_machines.png)
5. Для Proxmox в Influxdb поднимаем отдельный influxdb сервер в docker compose и создаем bucket с именем Proxmox и генерируем отдельный токен
6. В Proxmox переходим в раздел __Datacenter__ -> __Metric Server__ и добавляем influxdb
- ![Proxmox Influxdb Connection](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/proxmox_influxdb.png)
7. В Grafana создаем каталог Proxmox и импортируем Dashboard в формате json из каталога dashboards, в итоге получаем вот такую картинку:
- ![Proxmox Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/proxmox_dashboard.png)
8. В Grafana подключаем отправку уведомлений в Telegram с помощью токена
- ![Grafana Contact Points](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/grafana_contact_points.png)
9. Создаем алерты в телеграм, которые будут прилетать в случае, если виртуальные машины будут падать, либо будет высокая загрузка CPU или RAM на устройствах

#### Постановка на мониторинг Windows-серверов
1. Скачиваем MSI-установщик Windows Exporter ![по ссылке](https://github.com/prometheus-community/windows_exporter)
2. Устанавливаем на все серверы, по умолчанию он запустится на порту __9182__
![Windows Exporter](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/windows_server_exporter.png)
1. Добавляем на домен-контроллере правило, разрещающее входящее соединение на порт 9182, на остальные серверы создаем GPO (на OU Servers) с аналогичным правилом фаерволла. Если правило не создать, то подключение работать не будет
2. Добавляем в конфигурацию __prometheus.yml__ информацию по Windows экспортерам
```shell
- job_name: windows-exporters
    scrape_interval: 5s
    static_configs:
    - targets:
      - '192.168.61.10:9182'
      - '192.168.61.20:9182'
      - '192.168.61.15:9182'
      - '192.168.61.30:9182'
```
3. В Grafana создаем папку __Windows Servers__ и импортируем шаблон Windows Server, получаем вот такую картинку:
![Windows Servers Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/windows_servers_dashboard.png)
4. Устанавливаем telegraf на домен-контроллер и применяем конфигурацию __configs/win-telegraf.conf__ со скриптом __get_ad_security.ps1__, который будет собирать статистику по учетным записям из AD и выводить в дальнейшем на дашборд статистику
5. Импортируем дашборд __AD Security Stats__ и получаем такую картинку
![AD Security Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/ad_security_dashboard.png)
6. Настраиваем уведомление в телеграм, когда происходит блокировка УЗ. Настраиваем параметр блокировки УЗ в доменной пароль политики по умолчанию с помощью Powershell:
```powershell
Set-ADDefaultDomainPasswordPolicy -Identity "netlab.local" -LockoutThreshold 4

Get-ADDefaultDomainPasswordPolicy
ComplexityEnabled           : True
DistinguishedName           : DC=netlab,DC=local
LockoutDuration             : 00:10:00
LockoutObservationWindow    : 00:10:00
LockoutThreshold            : 4
MaxPasswordAge              : 42.00:00:00
MinPasswordAge              : 1.00:00:00
MinPasswordLength           : 7
objectClass                 : {domainDNS}
objectGuid                  : ba6e42dc-054a-4899-8308-43ac876c4931
PasswordHistoryCount        : 24
ReversibleEncryptionEnabled : False
```
Вводим 4 раза неправильно пароль на двух тестовых учетках и смотрим появилось ли уведомление в телеграме и есть ли изменения на графике
![AD Security Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/locked_accounts.png)
![AD Security Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/users_lockout_dashboard.png)
7. По истечении 10-и минут проверяем, что пришло уведомление и что график вернулся в прежнее состояние
![AD Security Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/unlocked_accounts.png)
![AD Security Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/users_unlockout_dashboard.png)

#### Постановка на мониторинг VPN-сервера Pritunl
1. Имеется VPN сервер на базе __OpenVPN__ - __Pritunl__ версии Enterprise
2. Устанавливаем __Node Exporter__ на сервер и добавляем ноду в конфигурацию с Prometheus (__configs/prometheus__)
3. В InfluxDB cоздаем __bucket__ с именем __pritunl__ и генерируем отдельный токен для него
![Создание Bucket](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/pritunl_bucket.png)
4. В настройках Pritunl указываем данные для подключения к InfluxDB
![Pritunl подключение к InfluxDB](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/pritunl_influx_connection.png)
5. Переходим в Grafana, создаем каталог VPN и импортируем в него два дашборда - один для самого VPN, который будет показывать статистику, а другой для мониторинга самой ноды
![Pritunl Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/pritunl_dashboard.png)
![VPN Server Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/vpn_server_dashboard.png)

#### Постановка на мониторинг Bookstack
1. Устанавливаем blackbox экспортер на ВМ с Bookstack и применяем конфигурацию __configs/blackbox.yml__
2. Далее в Grafana в папку app импортируем шаблон Bookstack, переименовываем его в Bookstack, на основе него будем делать свой кастомный дашборд
3. Создаем общий Dashboard __Bookstack App__ и добавляем в него графики из доступности и по книгам, получаем такую картину
![Bookstack Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/bookstack_monitoring.png)
![Bookstack Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/bookstack_http.png)

#### Сбор логов с сетевых устройств
1. Настраиваем логирование на сетевых устройствах следующим образом
```shell
# Cisco Devices
service timestamps log datetime localtime show-timezone
logging buffered 10000000
logging console critical
login on-failure log
login on-success log
logging facility local6
logging source-interface Vlan500 (на маршрутизаторе будет физический интерфейс, смотрящий в сторону ЛВС)
logging host 192.168.62.22

# Huawei Devices
info-center channel 7 name syslogng
info-center loghost 192.168.62.22 source-ip 192.168.50.55
info-center logbuffer size 1024
info-center timestamp log format-date

# b4com
logging buffer 1000
logging timestamp date
logging level module information
logging server enable
logging server facility local7
logging server address 192.168.62.22 source-interface vlan500
```
2. Устанавливаем на ВМ syslog-ng, создаем каталог __/var/log/netdevices__ и добавляем конфигурацию в __/etc/syslog-ng/conf.d/__ из __configs/netdevices.conf__
3. Устанавливаем Promtail и добавляем конфигурацию для захвата логов __/var/log/netdevices__ 
4. В Grafana создаём папку __Logs__, а в ней дашборд. В дашборде делаем панель с именем All logs и размещаем там все логи наших сетевых устройств
5. Создаем отдельную панель для отслеживания неудачных попыток входа по SSH. Запрос получился не совсем корректный, так как, если в файл не постоянно пишутся логи, то он долго будет висеть. Запрос выглядит следующим образом:
```sql
count_over_time({filename="/var/log/netdevices/192.168.50.1.log"} |= "SEC_LOGIN-4-LOGIN_FAILED"[5m])
```
Дашборд выглядит так:
![All Logs Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/network_devices_logs_dashboard.png)
6. Настроим алертинг для сетевых устройств, чтобы уходили уведомления в случае, если попыток за последние 5 минут было больше 5. Логинимся на коммутатор неудачно 6 раз и смотрим результат:
![All Logs Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/ssh_failed.png)
![All Logs Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/telegram_ssh_notifications.png)
![All Logs Dashboard](https://gitlab.com/ivan_storm/otus-obesrvability/-/raw/main/project/images/ssh_resolved.png)

#### Итог
В рамках обучения курса OTUS __"Observability: мониторинг, логирование и трейсинг"__ удалось изучить различные методы мониторинга, алертинга, логирования и трейсинга. На основе пройденного материала получилось применить полученные знания и навыки для применения в своей работе и постановке на мониторинг небольшой инфраструктуры тестовой лаборатории. Система мониторинга, алертинга и логирования будет улучшаться в дальнейшем для приведения ещё в более лучший вид.